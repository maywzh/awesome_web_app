#!/usr/bin/env python
# -*- coding: utf-8 -*-
from Model.Models import *

loop = asyncio.get_event_loop()

#插入
async def insert():
    await create_pool(loop,user='root', password='i698485876', db='awesome')
    u = User(name='Test2', email='test2@example.com', passwd='1234567890', image='about:blank')
    await u.save()
    r = await User.findAll()
    print(r)

# #删除
# async def remove():
#     await create_pool(loop, user='root', password='i698485876', db='awesome')
#     r = await User.find('001492757565916ec72f6eeb731405ab07ee37911a3ae79000')
#     await r.remove()
#     print('remove',r)
#     await destroy_pool()
#
# #更新
# async def update():
#     await create_pool(loop, user='root', password='i698485876', db='awesome')
#     r = await User.find('00149276202953187d8d3176f894f1fa82d9caa7d36775a000')
#     r.passwd = '765'
#     await r.update()
#     print('update',r)
#     await destroy_pool()


async def find():
    await create_pool(loop, user='root', password='i698485876', db='awesome')
    all = await User.findAll()
    print(all)
    pk = await User.find('00149276202953187d8d3176f894f1fa82d9caa7d36775a000')
    print(pk)
    num = await User.findNumber('email')
    print(num)
    await destroy_pool()

tasks= [insert(), find()]

loop.run_until_complete(asyncio.wait(tasks))
loop.close()
